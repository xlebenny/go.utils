package utils

import "testing"

func TestCamelCaseToUnderscore(t *testing.T) {
	type args struct {
		str         string
		escapeWords []string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "NoEscapeWord",
			args: args{str: "myID", escapeWords: []string{}},
			want: "my_i_d",
		},
		{
			name: "EmptyEscapeWord",
			args: args{str: "myID", escapeWords: []string{""}},
			want: "my_i_d",
		},
		{
			name: "SimpleCase",
			args: args{str: "myID", escapeWords: []string{"ID"}},
			want: "myID",
		},
		{
			name: "TwoEscapeWord",
			args: args{str: "myIDwordBENNYanotherWord", escapeWords: []string{"ID", "BENNY"}},
			want: "myIDwordBENNYanother_word",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := CamelCaseToUnderscore(tt.args.str, tt.args.escapeWords); got != tt.want {
				t.Errorf("CamelCaseToUnderscore() = %v, want %v", got, tt.want)
			}
		})
	}
}

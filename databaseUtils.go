package utils

import (
	"fmt"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mssql"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

// DatabaseConfig ...
type DatabaseConfig struct {
	DatabaseType DatabaseType
	Account      string
	Password     string
	Protocol     string
	IP           string
	Port         string
	DatabaseName string
	Parameters   string
	LogMode      bool
}

// DatabaseType ...
type DatabaseType string

const (
	// DatabaseTypeMySQL ...
	DatabaseTypeMySQL DatabaseType = "mysql"
	// DatabaseTypeMSSQL ...
	DatabaseTypeMSSQL DatabaseType = "mssql"
	// DatabaseTypePostgres ...
	DatabaseTypePostgres DatabaseType = "postgres"
)

func (DatabaseType DatabaseType) String() string {
	return string(DatabaseType)
}

// ConnectDatabase ...
func ConnectDatabase(config DatabaseConfig) (*gorm.DB, error) {
	connectionString := fmt.Sprintf(config.Account + ":" + config.Password + "@" + config.Protocol + "(" + config.IP + ":" + config.Port + ")/" + config.DatabaseName + "?" + config.Parameters)

	database, err := gorm.Open(config.DatabaseType.String(), connectionString)

	database.LogMode(config.LogMode)

	return database, err
}

# utils
[![build status](https://gitlab.com/xlebenny/go.utils/badges/master/build.svg)](https://gitlab.com/xlebenny/go.utils/commits/master)

Just a utils for myself

## utils.CamelCaseToUnderscore
Escape some word you don't want to underscore
`````
CamelCaseToUnderscore(str: "myID", escapeWords: []string{"ID"})
````
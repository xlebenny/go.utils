package utils

import (
	"strings"

	"github.com/asaskevich/govalidator"
	"github.com/satori/go.uuid"
)

// CamelCaseToUnderscore Just replace escape word then replace back
func CamelCaseToUnderscore(str string, escapeWords []string) string {
	result := str
	mapping := generateEscapeWordPair(escapeWords)

	// escapeWords --> uuid
	for key, value := range mapping {
		result = strings.Replace(result, value, key, -1)
	}

	result = govalidator.CamelCaseToUnderscore(result)

	// uuid --> escapeWords
	for key, value := range mapping {
		result = strings.Replace(result, key, value, -1)
	}

	return result
}

func generateEscapeWordPair(words []string) map[string]string {
	result := make(map[string]string, len(words))

	for _, x := range words {
		if len(x) == 0 {
			continue
		}

		// CamelCaseToUnderscore will replace "-"
		uuid := strings.Replace(uuid.NewV4().String(), "-", "", -1)
		result[uuid] = x
	}
	return result
}
